import React, {Component} from 'react'
import './Header.css'

class Header extends Component {
  render() {
    return (
      <header className="header">
        <div className="container">
          <h4>weatherbase <small className="t-muted t-thin">by Guilherme Oderdenge</small></h4>
        </div>
      </header>
    )
  }
}

export default Header
