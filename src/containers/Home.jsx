import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import './Home.css'

/* Containers */
import Header from './Header'

/* Components */
import Tile from 'components/Tile'
import Loading from 'components/shared/Loading'

/* Metas */
import cities from 'meta/cities'

/* Actions */
import {
  fetchCitiesWeatherIfNeeded,
  fetchCityForecast
} from 'actions/weatherActions'

class Home extends Component {
  componentDidMount() {
    const {dispatch} = this.props

    dispatch(fetchCitiesWeatherIfNeeded(cities))
  }

  render() {
    const {isFetching} = this.props.citiesWeather

    return (
      <div className="home">
        <Header />
        {isFetching ? <Loading center /> : this.renderTiles()}
      </div>
    )
  }

  renderTiles() {
    const {cities} = this.props.citiesWeather

    return (
      <div className="tiles">
        {cities.map(this.renderTile.bind(this))}
      </div>
    )
  }

  renderTile(city) {
    const {id, name, country, cover, forecast} = city
    const currentTemperature = city.main.temp // for intuitiveness
    const minTemperature = city.main.temp_min
    const maxTemperature = city.main.temp_max

    return (
      <Tile
        key={id}
        city={name}
        country={country}
        cover={cover}
        currentTemperature={currentTemperature}
        minTemperature={minTemperature}
        maxTemperature={maxTemperature}
        forecast={forecast}
        onClickShowForecast={this.onClickTile.bind(this, city)} />
    )
  }

  onClickTile(city) {
    const {dispatch} = this.props
    dispatch(fetchCityForecast(city))
  }
}

Home.propTypes = {
  citiesWeather: PropTypes.object,
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  const {citiesWeather} = state.weatherReducer
  return {citiesWeather}
}

export default connect(mapStateToProps)(Home)
