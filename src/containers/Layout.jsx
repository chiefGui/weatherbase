import React, {Component, PropTypes} from 'react'

class Layout extends Component {
  render() {
    const {children} = this.props

    return (
      <div className="layout">
        {children}
      </div>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node
}

export default Layout
