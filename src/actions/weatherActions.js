import superagent from 'superagent'
import find from 'lodash/collection/find'
import findIndex from 'lodash/array/findIndex'

import {
  resolveCurrentWeatherEndpointByCities,
  resolveForecastEndpointByCity
} from 'utils/resolver'

import merge from 'utils/merge'

export const REQUEST_CITIES_WEATHER         = 'REQUEST_CITIES_WEATHER'
export const REQUEST_CITIES_WEATHER_SUCCESS = 'REQUEST_WEATHER_SUCCESS'
export const REQUEST_CITIES_WEATHER_FAIL    = 'REQUEST_WEATHER_FAIL'

export function requestCitiesWeather(cities) {
  return {
    type: REQUEST_CITIES_WEATHER,
    cities: cities
  }
}

export function requestCitiesWeatherSuccess(cities) {
  return {
    type: REQUEST_CITIES_WEATHER_SUCCESS,
    cities: cities
  }
}

export function requestCitiesWeatherFail(cities, error) {
  return {
    type: REQUEST_CITIES_WEATHER_FAIL,
    cities: cities,
    error: error
  }
}

export function fetchCitiesWeatherIfNeeded(cities) {
  return (dispatch, getState) => {
    if (shouldFetchCitiesWeather(getState(), cities)) {
      return dispatch(fetchCitiesWeather(cities))
    }
  }
}

function fetchCitiesWeather(cities) {
  return dispatch => {
    dispatch(requestCitiesWeather(cities))

    return superagent.get(resolveCurrentWeatherEndpointByCities(cities))
      .end((error, response) => {
        if (error) {
          dispatch(requestCitiesWeatherFail(cities, error))
          return
        }

        const remoteCities = response.body.list
        const citiesWithWeather = merge.cities(cities, remoteCities)
        dispatch(requestCitiesWeatherSuccess(citiesWithWeather))
      })
  }
}

export const UPDATE_CITIES_LIST = 'UPDATE_CITIES_LIST'

function updateCitiesList(newCities) {
  return {
    type: UPDATE_CITIES_LIST,
    cities: newCities
  }
}

function updateCity(newCity) {
  return (dispatch, getState) => {
    const cities = getState().weatherReducer.citiesWeather.cities
    const toBeUpdatedCityIndex = findIndex(cities, {id: newCity.id})
    cities[toBeUpdatedCityIndex] = newCity

    dispatch(updateCitiesList(cities))
  }
}

export const REQUEST_CITY_FORECAST         = 'REQUEST_CITY_FORECAST'
export const REQUEST_CITY_FORECAST_SUCCESS = 'REQUEST_CITY_FORECAST_SUCCESS'
export const REQUEST_CITY_FORECAST_FAIL    = 'REQUEST_CITY_FORECAST_FAIL'

export function requestCityForecast(city) {
  return {
    type: REQUEST_CITY_FORECAST,
    city: city
  }
}

export function requestCityForecastSuccess(city) {
  return {
    type: REQUEST_CITY_FORECAST_SUCCESS,
    city: city
  }
}

export function requestCityForecastFail(city, error) {
  return {
    type: REQUEST_CITY_FORECAST_FAIL,
    city: city,
    error: error
  }
}

function shouldFetchCitiesWeather(state) {
  const {citiesWeather} = state.weatherReducer

  if (citiesWeather.cities.length === 0) { return true }
  if (citiesWeather.isFetching) { return false }

  return citiesWeather
}

export function fetchCityForecast(city) {
  return (dispatch, getState) => {
    dispatch(requestCityForecast(city))

    const {cities} = getState().weatherReducer.citiesWeather

    if (hasForecastAlready(cities, city)) { return }

    return superagent.get(resolveForecastEndpointByCity(city.name))
      .end((error, response) => {
        if (error) {
          dispatch(requestCityForecastFail(city, error))
          return
        }

        const forecast = response.body.list.slice(1, 4) // get the first three results since index 0
        const cityWithForecast = merge.cityForecast(cities, city.name, {forecast: forecast})

        dispatch(updateCity(cityWithForecast))
      })
  }
}

function hasForecastAlready(cities, city) {
  const currentCity = find(cities, {id: city.id})
  return !!currentCity.forecast
}
