import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import './Tile.css'

import moment from 'moment'

/* Utils */
import {convertFromKelvinToCelsius} from 'utils/converter'

class Tile extends Component {
  constructor() {
    super()

    this.state = {isShowingForecast: false}
  }

  render() {
    const style = {backgroundImage: `url('${this.props.cover}')`}

    return (
      <div className="tile">
        <div className="tile__content">
          <div className="wrapper">
            {this.renderHeader()}
          </div>

          {this.renderWeather()}
        </div>

        <div className="tile__overlay"></div>
        <div className="tile__image" style={style}></div>
      </div>
    )
  }

  renderHeader() {
    const {city, country} = this.props

    return (
      <header className="tile__header">
        <h2 className="tile__title t-thin">{city}</h2>
        <h4 className="tile__sub-title t-thin">{country}</h4>
      </header>
    )
  }

  renderWeather() {
    const {currentTemperature} = this.props

    const currentTemperatureInCelsius =
      convertFromKelvinToCelsius(currentTemperature)

    return (
      <div className="tile__weather">
        <h3 className="tile__current-temperature">{currentTemperatureInCelsius}&deg;</h3>

        {this.renderMinAndMaxTemperatures()}

        <button
          className="button button--primary"
          onClick={this.onShowForecast.bind(this)}>
          {this.state.isShowingForecast ? 'Hide forecast' : 'Show forecast'}
        </button>

        {this.state.isShowingForecast && this.renderForecast()}
      </div>
    )
  }

  renderForecast() {
    const {forecast} = this.props

    if (!forecast) { return }

    return (
      <div className="tile__forecast">
        {forecast.map((forecastItem, index) => {
          const date = moment(forecast[index].dt_txt).format('dddd, HH:mm')

          return (
            <div key={index} className="forecast__item">
              <sup className="t-sup">{date}h</sup>
              <span>{convertFromKelvinToCelsius(forecast[index].main.temp)}&deg;</span>
            </div>
          )
        })}
      </div>
    )
  }

  renderMinAndMaxTemperatures() {
    const {minTemperature, maxTemperature} = this.props

    const minTemperatureInCelsius =
      convertFromKelvinToCelsius(minTemperature)

    const maxTemperatureInCelsius =
      convertFromKelvinToCelsius(maxTemperature)

    return (
      <div className="tile__temperatures">
        <div className="tile__temperature tile__max-temperature">
          <sup className="t-sup">min</sup>
          {minTemperatureInCelsius}&deg;
        </div>

        <div className="tile__temperature tile__max-temperature">
          <sup className="t-sup">max</sup>
          {maxTemperatureInCelsius}&deg;
        </div>
      </div>
    )
  }

  onShowForecast() {
    const {isShowingForecast} = this.state
    this.setState({isShowingForecast: !isShowingForecast})

    this.props.onClickShowForecast()
  }
}

Tile.propTypes = {
  city: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  currentTemperature: PropTypes.number.isRequired,
  minTemperature: PropTypes.number.isRequired,
  maxTemperature: PropTypes.number.isRequired,

  cover: PropTypes.string,
  forecast: PropTypes.array,

  dispatch: PropTypes.func.isRequired,
  onClickShowForecast: PropTypes.func
}

const mapStateToProps = state => {
  const {cities} = state.weatherReducer.citiesWeather
  return {cities}
}

export default connect(mapStateToProps)(Tile)
