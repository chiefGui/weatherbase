const KELVIN_SUBTRACTION_FORMULA_VALUE = 273.15

export function convertFromKelvinToCelsius(value) {
  return Math.floor(value - KELVIN_SUBTRACTION_FORMULA_VALUE)
}
