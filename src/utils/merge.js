import filter from 'lodash/collection/filter'
import merge from 'lodash/object/merge'

export default {
  cities(localCities, remoteCities) {
    return filter(localCities, localCity => {
      return remoteCities.map(remoteCity => {
        if (localCity.id === remoteCity.id) {
          merge(localCity, remoteCity)
        }
      })
    })
  },

  cityForecast(inCacheCities, selectedCity, forecast) {
    return filter(inCacheCities, city => {
      if (city.name === selectedCity) {
        return merge(city, forecast)
      }
    })[0]
  }
}
