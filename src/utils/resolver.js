import pluck from 'lodash/collection/pluck'

// In a real scenario, the value of the API_KEY is limited to an environment
// variable for security reasons.
const API_KEY = '2de143494c0b295cca9337e1e96b00e0'

export const resolveCurrentWeatherEndpointByCities = cities => {
  const citiesId = pluck(cities, 'id').join(',')
  return `http://api.openweathermap.org/data/2.5/group?id=${citiesId}&appid=${API_KEY}`
}

export const resolveForecastEndpointByCity = city => {
  return `http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${API_KEY}`
}
