import {combineReducers} from 'redux'
import {
  REQUEST_CITIES_WEATHER,
  REQUEST_CITIES_WEATHER_SUCCESS,
  REQUEST_CITIES_WEATHER_FAIL,

  UPDATE_CITIES_LIST,

  REQUEST_CITY_FORECAST,
  REQUEST_CITY_FORECAST_SUCCESS,
  REQUEST_CITY_FORECAST_FAIL
} from 'actions/weatherActions'

function citiesWeather(state = {
  isFetching: false,
  error: false,
  cities: []
}, action) {
  switch (action.type) {
    case REQUEST_CITIES_WEATHER:
      return Object.assign({}, state, {
        isFetching: true,
        error: false
      })
    case REQUEST_CITIES_WEATHER_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        cities: action.cities
      })
      case REQUEST_CITIES_WEATHER_FAIL:
        return Object.assign({}, state, {
          isFetching: false,
          error: true,
          cities: action.cities
        })
      case UPDATE_CITIES_LIST:
        return Object.assign({}, state, {
          isFetching: false,
          error: false,
          cities: action.cities
        })
      default:
        return state
  }
}

function citiesForecasts(state = {
  isFetching: false,
  error: false,
  city: {}
}, action) {
  switch (action.type) {
    case REQUEST_CITY_FORECAST:
      return Object.assign({}, state, {
        isFetching: true,
        error: false
      })
    case REQUEST_CITY_FORECAST_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        error: false,
        city: action.city
      })
    case REQUEST_CITY_FORECAST_FAIL:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      })
    default:
      return state
  }
}

const weatherReducer = combineReducers({citiesWeather, citiesForecasts})

export default weatherReducer
