export default [
  {
    id: 2759794,
    name: 'Amsterdam',
    country: 'Netherlands',
    cover: 'http://www.hdtimelapse.net/content/HDtimelapse.net_City/HDtimelapse.net_City_0510_vertical.jpg'
  },
  {
    id: 2643743,
    name: 'London',
    country: 'England',
    cover: 'https://images.unsplash.com/photo-1449094013570-5fe3fa269b2e?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=2611da7b057b65b687e22af22f29467f'
  },
  {
    id: 2988507,
    name: 'Paris',
    country: 'France',
    cover: 'http://24.media.tumblr.com/9b1133eb61cd39d335c90f1cb1a34d2b/tumblr_mxvvrn8rf61qiaclwo1_500.jpg'
  },
  {
    id: 3067696,
    name: 'Prague',
    country: 'Czech Republic',
    cover: 'http://www.aateampro.ca/pix/photos/prague-vertical.jpg'
  },
  {
    id: 2800866,
    name: 'Brussels',
    country: 'Belgium',
    cover: 'http://www.orangesmile.com/common/img_final_large/brussels_sightseeing.jpg'
  }
]
