import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'

import rootReducer from 'reducers/Root'

const finalCreateStore = compose(applyMiddleware(thunk))(createStore)

export default function Store(initialState) {
  return finalCreateStore(rootReducer, initialState)
}
