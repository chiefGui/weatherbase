# Backbase Weather Application Challenge

A simple weather application that displays the climate conditions across five
cities in Europe: Amsterdam, London, Paris, Prague and Brussels.

### Installing

After cloning the application, all you need to do to get it running is:

```shell
npm install
```

```shell
npm run dev
```

Then the app will be running at `http://localhost:8090`.

Or, if you want a standalone version settled in a production environment:

```shell
npm run build && open build/index.html
```

### Technologies

I picked a few technologies that I feel comfortable to play with:

- React: since I've been working on component-driven development last one
year and a half, I thought it was a wise decision for me to write the user
interface of this application entirely on the fantastic Facebook's technology.

  Its main achievement is dealing with all dynamic stuff that the user is capable to see and interact with—React is handling all events and all the interface as well: everything have life.

- Redux: React by itself isn't enough when dealing with smart applications. In the real world, data is dynamic and changes every time—then a decision needs to be taken: use something simple, yet powerful to handle with persistent pieces of data originating from an endpoint—as purposed by the challenge.

  React for the interface, Redux for the data.

- Webpack: I decided to not use a task runner. Instead, a bundler only—no further need for something like Gulp or Grunt (except if I would to deploy or something). Also, integrating loaders as Babel is a simple task here.

- superagent: neither React or Redux do HTTP requests by their own. To deal with asynchronous requests over HTTP, I decided to use superagent due to its simplicity and promise-based API.

- lodash: such an incredible toolbelt to do simple tasks without reinveinting the wheel each time it is needed, like filtering or plucking a collection.

- es6 (transpiled by Babel): the next step JavaScript is taking. Unlike TypeScript or CoffeeScript, this **is** JavaScript—when es6 becomes a standard, the application will be prepared just as me as a JavaScript developer.

### About the CSS

At first, you can feel uncomfortable seeing the way I wrote CSS—but don't be scared! I am using [BEM](https://en.bem.info/), a methodology to design CSS in a semantic way that speed the development process, specially working with a team since its naming convention is developer-friendly and have reusability in mind.

Basically, I've been working with it since the end of 2014 and it's hard to get
rid of the habit: its advantages when working with component-driven application
don't let me take it away.

Since day one working with BEM (the mindset specifically), I realized that there's no need for using a CSS preprocessor like SASS or LESS. The greatest advantage of it is transferring the responsibility of making something visually reusable to the HTML—the place it needs to be.


### The way the code is written

Everything I wrote has a purpose—from the naming conventions I use, until the
sequence the code appears in the text editor.

For instance, the properties I wrote as rules on any property in the CSS files
has a meaningful sequence: [they are grouped based on their importance](https://css-tricks.com/poll-results-how-do-you-order-your-css-properties/).

To have a better comprehension, the rules are grouped. As being not enough, they are ordered by importance. Your element can survive without a `color` property, but could it without the `position: absolute`?

A few important things to highlight:

- CSS rules are placed in the same order as their elements are appearing on the HTML;
- JavaScript methods are ordered based on their invocation sequence. The method that unleashes another comes first and go on;

### Responsiveness

The application is _almost_ responsive. I tried to do something that can give you an idea about the possibilities, yet it is not totally and perfectly responsive.

For the typography, I am using `rem` as the definitive unit since it's easy to manage across the different scenarios the font design can face.

### General

I think that's it. If you have any questions, don't hesitate to ping me up!
